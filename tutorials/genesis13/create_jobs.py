# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 06:27:51 2023

@author: lixiangk
"""

import shutil
from interface import *
#from Job import *

def post_job(Ipeak, Qtot):
    direc = 'beam_%.0fA_%.1fnC' % (Ipeak, Qtot); print(direc)
    os.chdir(direc)
    
    os.system('cp ../post.py ./post.py')
    #job = QsubJob(command = 'python')
    #job.create(jobName = 'tmp', inputName = '$Py/postGenesis_qsub.py', submit = True)
    
    job = CondorJob(command = 'python')
    job.create(jobName = 'temp.sh', inputName = '../post.py', submit = True)
    
    os.chdir('..')
    
    return

### Scan 
currents = [50, 100, 150, 200]
currents = [0.3]

charges = np.linspace(1.5, 5, 8)
charges = [0.001]

seeds = np.arange(20)+1
#seeds = [1]

combi = np.array([[v1, v2, v3] for v1 in currents for v2 in charges for v3 in seeds])
#combi = [[78, 1], [100, 1.5]]
#combi = [[0.3, 0.001, 1]]

version = 4
for v in combi:
    
    Ipeak, Qtot, seed = v[:] # A, nC, iprime
    #xc, xcp, yc, ycp, seed = v[:]
    
    if version == 3:
        inputName = 'run_job3.py'
        loadEnv = 'module purge && module add python/3.9 mpi/openmpi-x86_64 phdf5/1.14.4 szip/2.1.1'
    elif version == 4:
        inputName = 'run_job4.py'
        #loadEnv = 'module purge && module add python/3.9 mpi/openmpi-x86_64 phdf5/1.14.4-2 szip/2.1.1 genesis/4.6.6a'
        loadEnv = 'module purge && module add python/3.9 genesis/4.6.6c'
    
    #jobName = '1_%.2fmm_%.2fmrad_%.2fmm_%.2fmrad_%d' % (xc*1e3, xcp*1e3, yc*1e3, ycp*1e3, seed);
    direc = 'beam_%.2fA_%.2fpC' % (Ipeak, Qtot*1e3)
    direc = 'test2'
    
    CreateFolder(direc)
    
    # Make local copies for the jobs
    os.chdir(direc)
    shutil.copy(os.path.abspath(__file__), '.')
    
    shutil.copy('..'+os.sep+inputName, '.')
    ###
    
    jobName = direc+f'_{seed}'
    
    # simulation
    job = CondorJobNew(command = 'python', request_cpus = 1)
    job.create(jobName+'.sh', inputName, args = v, submit = True, loadEnv = loadEnv)
    ###
    
    os.chdir('..')
#exit()

#%%
