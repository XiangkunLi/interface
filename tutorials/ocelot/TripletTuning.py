import sys

from scipy.interpolate import interp1d, interp2d
import matplotlib as mpl
import matplotlib.pyplot as plt
#plt.switch_backend('agg')

import timeit, math
import shutil, os

# Get the directory of the currently running script
scriptdir = os.path.dirname(os.path.abspath(__file__))

# Add this directory to sys.path if it's not already there
if scriptdir not in sys.path:
    sys.path.insert(0, scriptdir)

# Now you can import the module as usual
from ObjOcelot import *

#%% Use or not use MPI
rank = 0
isParallel = False
try:
    from mpi4py import MPI
    isParallel = True
except Exception as err:
    print(err)
    
#%% Go the the working folder
workdir = r'\\afs\ifh.de\group\pitz\data\lixiangk\sim3\2025\MatchingChicane'
os.chdir(workdir)

if __name__ == "__main__":

    if isParallel:
        comm = MPI.COMM_WORLD
        size = comm.Get_size()
        rank = comm.Get_rank()
    
    if True:
        print(sys.argv)
        
        grad1 = 1.1 # gradient of the first quad
        if len(sys.argv)>1:
            grad1 = np.float(sys.argv[1])
        grad2 = -1.5*grad1
        #grad2 = -4.563
        
        grad3 =  grad1*2
        if len(sys.argv)>2:
            grad2 = np.float(sys.argv[2])
        if len(sys.argv)>3:
            grad3 = np.float(sys.argv[3])
        print(grad1, grad2, grad3)

        ### Parameter Setup here
        z0 = 0 # position of initial beam
        
        Run0 = 1
        fname0 = 'matched@sample.2809.002'
        print(fname0)

    
        quadNames = ['Back.Q1', 'Back.Q2', 'Back.Q3']
        Zstop = pitz['back.Q3']+1
        
        Zfinal = scrns['Back.SCR2']
        ###
        
        ### Control of scan, not used with the current tuning strategy
        Q2_scanned, Q2_found = 0, 1
        Q3_scanned, Q3_found = 0, 0
        ###
        
        zquad = [quads[name] for name in quadNames] # positions of quads to be tuned
        zmidd = int((zquad[1]+zquad[2])*5)/10. # output position between the second and third quads
        
        # Parameter scan
        var2 = np.array([0.25, 0.5, 0.75, 0.9, 1.0, 1.1, 1.25, 1.50])*grad2
        combi = np.array([[grad1, v2, 0] for v2 in var2])
                
        args = (quadNames,)
        Distribution = '..'+os.sep+fname0
        kwargs = {'Run':Run0+1, 'Distribution':Distribution, 'Zstart':z0,
                  'Zstop':zquad[-1]+1e-6, 'step':5, 'Screen':[zquad[2]]}
        
        def func_wrapper(x):
            r = 0
            r = ObjOcelot(x, zquad[:-1], **kwargs, obj_fun = get_obj2)
            return [r]
        
        # parallel mapping
        if not Q2_scanned:
            if isParallel:
                
                m = int(math.ceil(float(len(combi))/size))
                x_chunk = combi[rank*m:(rank+1)*m]
                r_chunk = list(map(func_wrapper, x_chunk))

                # collect results
                r = comm.allreduce(r_chunk)
                
            else:
                if 1:
                    res = []
                    #grad2 = -grad1
                    y = [grad1, grad2, 0]
                    result = func_wrapper(y)
                    res.append(result[0])
                    
                    nTrials, maxTrials = 0, 5
                    while nTrials<maxTrials:
                        nTrials += 1
                        if len(res)>=2:
                            data = np.array(res)
                            data = data[np.abs(data[:,1]).argsort()[::]]
    
                            func = interp1d(1.0-data[:,7]/data[:,8], data[:,1], fill_value = 'extrapolate')
                            func = interp1d(data[:,7]-data[:,8], data[:,1], fill_value = 'extrapolate') # rms size match
                            #func = interp1d(data[:,10]-data[:,11], data[:,1], fill_value = 'extrapolate') # beta match
                            tmp = func([0])[0]                            
                
                        else:
                            if result[0][3]*np.sign(grad1)>0:
                                tmp = grad2*0.75
                            else:
                                tmp = grad2*1.25
                    
                        direc = ''
                        for i, xi in enumerate(y):
                            direc += str.format('Q%d-%.2fT_m-' %  (i+1, xi))
                        direc = direc[:-1]
                        shutil.rmtree(direc)
                        
                        if np.abs(1-tmp/grad2)<1e-3:
                            print(('Grad2 = %.4f T/m' % grad2))
                            break
                        else:
                            grad2 = tmp
                            
                        y = [grad1, grad2, 0]
                        result = func_wrapper(y)
                        res.append(result[0])
                    
                else:
                    r = []
                    for x in combi:
                        result = func_wrapper(x)
                        r.append(result)
            
            fig, ax = plt.subplots()
            ax.plot(data[:,1], data[:,7], '-*')
            ax.plot(data[:,1], data[:,8], '-o')
            ax.grid()
            ax.set_xlabel(r'$G$ (T/m)')
            ax.set_ylabel(r'rms size (mm)')
            fig.savefig('rms-size-vs-grad2@%.2fT_m.png' % grad1)

        # else:
        #     grad1 = None #comm.bcast(grad1, root = 0)
        #     x = None #comm.bcast(x, root = 0)
            
        if isParallel:
            grad1 = comm.bcast(grad1, root = 0)
            grad2 = comm.bcast(grad2, root = 0)
            x = comm.bcast(y, root = 0)
            
        # Find Q3
        fname1 = fname0
        Distribution = '..'+os.sep+fname1

        # Parameter scan
        var3 = np.linspace(0.5, 2.0, 8)*grad3
        var3 = np.array([0.25, 0.5, 0.75, 0.9, 1.0, 1.1, 1.25, 1.50])*grad3
        combi = np.array([[grad1, grad2, v3] for v3 in var3])
        
        kwargs = {'Run':Run0+2, 'Distribution':Distribution, 'Zstart':z0,
                  'Zstop':Zstop, 'step':2, 'Screen':[Zstop],
                  'zmin':zquad[-2]-z0}
        def func_wrapper(x):
            r = 0
            r = ObjOcelot(x, zquad, **kwargs, obj_fun = get_obj3)
            return [r]

        if not Q3_scanned:
            if isParallel:
                m = int(math.ceil(float(len(combi)) / size))
                x_chunk = combi[rank*m:(rank+1)*m]
                r_chunk = list(map(func_wrapper, x_chunk))
    
                r = comm.allreduce(r_chunk)
            else:
                if 0:
                    def obj_func(x):
                        y = [grad1, x[0], 0]
                        result = func_wrapper(y); print(result)
                        obj = result[0][3]**2
                        return obj
                    from scipy.optimize import minimize
                    grad2 = -grad1
                    init = np.array([[grad2], [grad2*1.5]])
                    res = minimize(obj_func, [grad2], method='Nelder-Mead', 
                                   tol=1e-6, options = {'maxiter':100, 'initial_simplex':init})
                elif 1:
                    res = []
                    #grad3 = grad1
                    y = [grad1, grad2, grad3]
                    result = func_wrapper(y)
                    res.append(result[0])
                    
                    while 1:
                        if len(res)>=2:
                            data = np.array(res)
                            
                            select = (data[:,3]<1e4); data = data[select]
                            data = data[np.abs(data[:,2]).argsort()[::]]
    
                            #func = interp1d(data[:,3], data[:,2], fill_value = 'extrapolate') # rms size match
                            func = interp1d(1.0-data[:,7]/data[:,8], data[:,2], fill_value = 'extrapolate')
                            func = interp1d(data[:,7]-data[:,8], data[:,2], fill_value = 'extrapolate') # rms size match
                            #func = interp1d(data[:,10]-data[:,11], data[:,2], fill_value = 'extrapolate') # beta match
                            tmp = func([0])[0]                            
                
                        else:
                            if result[0][3]*np.sign(grad1)>0:
                                tmp = grad3*1.25
                            else:
                                tmp = grad3*0.75
                    
                        if np.abs(1-tmp/grad3)<5e-3:
                                print(('Grad3 = %.6f T/m' % grad3))
                                break
                        else:
                            direc = CreateFolderName(y, flag = 'transport')
                            shutil.rmtree(direc)
                            grad3 = tmp
                            
                        y = [grad1, grad2, grad3]
                        result = func_wrapper(y)
                        res.append(result[0])
                    
                else:
                    r = []
                    for x in combi:
                        result = func_wrapper(x)
                        r.append(result)
                        
            fig, ax = plt.subplots()
            ax.plot(data[:,2], data[:,10], '-*')
            ax.plot(data[:,2], data[:,11], '-o')
            ax.grid()
            ax.set_xlabel(r'$G$ (T/m)')
            ax.set_ylabel(r'rms size (mm)')
            fig.savefig('rms-size-vs-grad3@%.2fT_m.png' % grad1)

            with open('results.dat','a') as f_handle:
                np.savetxt(f_handle,np.atleast_2d(y),fmt='%14.6E')
    
    plt.close('all')
    direc = CreateFolderName(y, flag = 'transport')
    
    if Zfinal > 0:
        #shutil.rmtree(direc)
    
        Zstop = Zfinal
        def func_wrapper(x):
            r = ObjOcelot(x, zquad, Distribution = Distribution, Zstart = z0,
                        Zstop = Zstop, obj_fun = get_obj2, save = True, 
                        Distribution1 = 'ast.%04d.001' % (Zfinal*1e2))
            return [r]
        
        y = [grad1, grad2, grad3]; 
        #y = [1.5, -4.5641517909965055, 3]
        result = func_wrapper(y)

    print('Setting found: ', y)
    # make plot
    # make_plot(y, z0 = z0)
