# -*- coding: utf-8 -*-
"""
Created on Sat Feb 22 12:20:43 2025

@author: lixiangk
"""

from interface import *

Distribution = 'Beam.ini'
generator = Generator1(FNAME = Distribution, 
                       IPart = 250000, 
                       Species = 'electrons', 
                       Q_total = 250e-3,
                       Ref_Ekin = 0.0e-6, 
                       LE = 0.55e-3*1, 
                       dist_pz = 'i',
                       Dist_z = 'g', 
                       sig_clock = 10e-3/2.355, 
                       Cathode = True,
                       Dist_x = '2', 
                       sig_x = 1.2/4, 
                       Dist_px = 'g',
                       Nemit_x = 0,
                       Dist_y = '2', 
                       sig_y = 1.2/4, 
                       Dist_py = 'g', 
                       Nemit_y = 0,
                       C_sig_x = 0,
                       C_sig_y = 0)

newrun = Namelist('Newrun', 
                  Run = Run, 
                  Head = 'PITZ beam line simulation', 
                  Distribution = Distribution, 
                  CathodeS = True,
                  Auto_Phase = True, 
                  Track_All = True, 
                  check_ref_part = False, 
                  Lprompt = False, 
                  Max_step=200000)

charge = Namelist('Charge', 
                  LSPCH = True, 
                  Lmirror = True, 
                  Nrad = 40, 
                  Nlong_in = 50, 
                  N_min = 50, 
                  Max_scale = 0.05, 
                  Max_count = 20)
charge.set(N_min= 100)

cavity = Namelist('Cavity', 
                  LEfield = True, 
                  File_Efield = ['gun51cavity.txt', 
                                 'CDS14_15mm.txt'],
                  MaxE = [60, 14], 
                  C_pos = [0., 2.675], 
                  Nue = [1.3, 1.3], 
                  Phi = [0, 0])

soleno = Namelist('Solenoid', 
                  LBfield = True, 
                  File_Bfield = ['gunsolenoidsPITZ.txt'],
                  MaxB = [-0.2], 
                  S_pos = [0.], 
                  S_xrot = [0*1e-3], 
                  S_yrot = [0])

Zstart, Zstop = 0, 5.28
Zemit = int((Zstop-Zstart)/0.01)
output = Namelist('Output', 
                  Zstart = Zstart, 
                  Zstop = Zstop, 
                  Zemit = Zemit, 
                  Zphase = 1, 
                  RefS = True, 
                  EmitS = True,
                  PhaseS = True, 
                  TrackS = False, 
                  LandFS = True, 
                  C_EmitS = True, 
                  LPROJECT_EMIT = True,
                  LOCAL_EMIT = False, 
                  Screen = [5.28])

apertu = Namelist('Aperture', 
                  LApert = True, 
                  File_Aperture = ['app.txt'])

quadru = Namelist('Quadrupole', 
                  LQuad = True, 
                  Q_type = ['Qsol.data'], 
                  Q_pos = [0], 
                  Q_grad = [1])

astra = Astra()
astra.add_modules([newrun, charge, cavity, soleno, output])


